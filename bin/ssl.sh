#!/bin/sh
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
  -keyout /etc/ssl/private/nginx-selfsigned.key \
  -out /etc/ssl/certs/nginx-selfsigned.crt \
  -subj "/C=AR/ST=Buenos Aires/L=Buenos Aires/O=Internet Widgits Pty Ltd/CN=appster.freisinger.com.ar/emailAddress=afreisinger@gmail.com"