#!/bin/sh

FPORT=1025;
EPORT=9999;
RANDHTTP=$(( ( RANDOM % $FPORT )  + $EPORT ))

# Name of the Docker container provided in ARG $1
NAME=$1
IMAGE=$2

check_port_availability () {

	HTTP_PORT_CHECK=$1
	
	#for USED_PORT in $( netstat -ltn | sed -rne '/^tcp/{/:\>/d;s/.*:([0-9]+)\>.*/\1/p}' | sort -n | uniq ); do
        for USED_PORT in $( netstat -ltn | grep -E 'tcp|udp' | awk -F\  '{print $4}' | awk -F. '{print $5}'| sort -n | uniq ); do
                if [ $HTTP_PORT_CHECK -eq $USED_PORT ]; then
                        printf "\n\n$HTTP_PORT_CHECK conflicts with open port: $USED_PORT...Randomizing HTTP Port!\n\n"
                        RANDHTTP=$(( ( RANDOM % $FPORT )  + $EPORT ))
                        exit
                fi
        done
        return
}

port_sanity=$(check_port_availability $RANDHTTP)

# Port check and randomize
# Loop until all ports are random
if [ -z "$port_sanity" ]; then
        printf "\nWe will run the container with these randomly assigned ports:\nHTTP port $RANDHTTP\n"
else
	port_sanity=$(check_port_availability $RANDHTTP)
fi
# Run container
# Make sure this Container is not running
printf "Make sure a Container with the designated name is not running..."
OUTPUT="$(docker stop $NAME)"
if echo "$OUTPUT" | grep -c "No such container"; then
        echo "A container with name, $NAME, was stopped. Good to proceed.."
else
	echo "No container with name, $NAME, exists. Good to proceed.."

fi

printf "\nGoing to run:\ndocker run -d -p $RANDHTTP:80 --name $NAME $IMAGE\n\n"

docker load --input .ci_exports/$CI_COMMIT_REF_SLUG.tar 
docker run -d -p $RANDHTTP:80 --name $NAME $IMAGE

printf "\nGoing to run:\ndocker exec $NAME nginx -t\n\n"

OUTPUT="$(docker exec $NAME nginx -t)"

if echo "$OUTPUT" | grep -c "test is successful"; then
        echo "nginx: test is unsuccessful"
        echo unsuccessful .ci_status/test_config   
else
        echo "nginx: the configuration file /etc/nginx/nginx.conf syntax is ok"
        echo "nginx: configuration file /etc/nginx/nginx.conf test is successful"
        echo successful .ci_status/test_config
fi
exit
